<?php

/**
 * Created by PhpStorm.
 * User: Paweł
 * Date: 2017-05-30
 * Time: 16:14
 */
require_once('DB.php');

class ServiceFunction
{

    public function getInitials($first_name, $last_name)
    {
        $name = '';
        $name .= strtoupper((substr($first_name, 0, 1)));
        $name .= '' . strtoupper(substr($last_name, 0, 1));
        return $name;
    }

    public function checkProduct($id)
    {
        $DB = new DB('localhost', 'root', '', 'phpcamp_pkolasinski');
        $DB->query('SELECT * FROM `products` WHERE id = '. $id .';');
        return $DB->getAllRows();
    }

    public function addProduct($name, $price)
    {
        $DB = new DB('localhost', 'root', '', 'phpcamp_pkolasinski');
        if($DB->query('INSERT INTO `products` (name, price) VALUES ("'. $name .'","'. $price. '");'))
        {
            return 'Dodano produkt o nazwie ' . $name;
        }
        return "Error";
    }

    public function removeProduct($id)
    {
        $DB = new DB('localhost', 'root', '', 'phpcamp_pkolasinski');
        if($DB->query('DELETE FROM `products` WHERE id = '. $id .';'))
        {
            return "Usunięto produkt o id = " . $id;
        }
    }
}

$options = array('uri' => 'http://localhost:8080/');
$server = new SOAPServer(NULL, $options);
$server->setClass('ServiceFunction');
$server->handle();