<?php
require_once('DB.interface.php');

class DB implements iDB {

    private $conn;
    private $result;

    public function __construct($host, $login, $password, $dbName)
    {
        $this->conn = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->conn) {
            throw new Exception ('Nie udało się połączyć z bazą danych: ' . $dbName);
        }
    }

    public function query($query)
    {
        $this->result = mysqli_query($this->conn, $query);

        return (bool)$this->result;
    }

    public function getAffectedRows()
    {
        return mysqli_num_rows($this->result);
    }

    public function getRow()
    {
        if (!$this->result) {
            throw new Exception('Zapytanie nie zostało wykonane');
        }
        return mysqli_fetch_row($this->result);
    }

    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }

    public function getWholeTable($table){
        $this->result = mysqli_query($this->conn, "'SELECT * FROM `". $table ."'");
    }

}

//$DB = new DB('localhost', 'root', '', 'phpcamp_pkolasinski');
//$DB->query('SELECT * FROM `clients` LIMIT 1000');

//print $DB->getAffectedRows() . '<br />';
//var_dump($DB->getRow());
//echo '<br /';
//var_dump($DB->getAllRows());

?>